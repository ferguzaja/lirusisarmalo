/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

/**
 *
 * @author ferguzaja
 */
public class MenuAdministrador extends javax.swing.JFrame {

    /**
     * Creates new form MenuAdministrador
     */
    public MenuAdministrador() {
        initComponents();
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JBEmpleadps = new javax.swing.JButton();
        JBClientes = new javax.swing.JButton();
        JBProductos = new javax.swing.JButton();
        JBEstatus = new javax.swing.JButton();
        JBPedido = new javax.swing.JButton();
        JBBuscar = new javax.swing.JButton();
        jLabelFondo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        JBEmpleadps.setText("Empleados");
        JBEmpleadps.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEmpleadpsActionPerformed(evt);
            }
        });
        getContentPane().add(JBEmpleadps, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, -1));

        JBClientes.setText("Clientes");
        JBClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBClientesActionPerformed(evt);
            }
        });
        getContentPane().add(JBClientes, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, 80, -1));

        JBProductos.setText("Productos");
        JBProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBProductosActionPerformed(evt);
            }
        });
        getContentPane().add(JBProductos, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 230, -1, -1));

        JBEstatus.setText("Cambiar Estatus");
        JBEstatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBEstatusActionPerformed(evt);
            }
        });
        getContentPane().add(JBEstatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 160, -1, -1));

        JBPedido.setText("Realizar Pedido");
        JBPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBPedidoActionPerformed(evt);
            }
        });
        getContentPane().add(JBPedido, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 100, -1, -1));

        JBBuscar.setText("Buscar Pedido");
        JBBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBBuscarActionPerformed(evt);
            }
        });
        getContentPane().add(JBBuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 240, -1, -1));

        jLabelFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/GUI/images/meme-liru-sisa-7.jpg"))); // NOI18N
        getContentPane().add(jLabelFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void JBEmpleadpsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEmpleadpsActionPerformed
        // TODO add your handling code here:
        new Empleados();
    }//GEN-LAST:event_JBEmpleadpsActionPerformed

    private void JBClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBClientesActionPerformed
        // TODO add your handling code here:
        new Clientes();
    }//GEN-LAST:event_JBClientesActionPerformed

    private void JBProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBProductosActionPerformed
        // TODO add your handling code here:
        new Producto();
    }//GEN-LAST:event_JBProductosActionPerformed

    private void JBPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBPedidoActionPerformed
        // TODO add your handling code here:
        new Pedido();
    }//GEN-LAST:event_JBPedidoActionPerformed

    private void JBEstatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBEstatusActionPerformed
        // TODO add your handling code here:
        new Estatus();
    }//GEN-LAST:event_JBEstatusActionPerformed

    private void JBBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBBuscarActionPerformed
        // TODO add your handling code here:
        new Buscar();
    }//GEN-LAST:event_JBBuscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MenuAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MenuAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MenuAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MenuAdministrador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MenuAdministrador().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBBuscar;
    private javax.swing.JButton JBClientes;
    private javax.swing.JButton JBEmpleadps;
    private javax.swing.JButton JBEstatus;
    private javax.swing.JButton JBPedido;
    private javax.swing.JButton JBProductos;
    private javax.swing.JLabel jLabelFondo;
    // End of variables declaration//GEN-END:variables
}
